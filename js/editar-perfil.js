$(document).ready(function() {
// Get buttons
var btn_step1 = document.getElementById("step1");
var btn_step2 = document.getElementById("step2");
var btn_step3 = document.getElementById("step3");

$("#e1").select2({
	placeholder: "¿En cuál plataforma vendes?"
	//templateResult: formatState
});

// Go to step 1
btn_step1.onclick = function() {
	// hide old forms
	$("#step2Rules").addClass("d-none");
	$("#step3Rules").addClass("d-none");
	// show new form
	$("#step1Rules").removeClass("d-none");
	// select button
	$("#step1").addClass("btn-selected");
	// unselect other buttons
	$("#step2").removeClass("btn-selected");
	$("#step3").removeClass("btn-selected");
	// add icon
	$("#icon-step1").removeClass("invisible");
	// remove icons
	$("#icon-step2").addClass("invisible");
	$("#icon-step3").addClass("invisible");
}

// Go to step 2
btn_step2.onclick = function() {
	// hide old forms
	$("#step1Rules").addClass("d-none");
	$("#step3Rules").addClass("d-none");
	// show new form
	$("#step2Rules").removeClass("d-none");
	// select button
	$("#step2").addClass("btn-selected");
	// unselect other buttons
	$("#step1").removeClass("btn-selected");
	$("#step3").removeClass("btn-selected");
	// add icon
	$("#icon-step2").removeClass("invisible");
	// remove icons
	$("#icon-step1").addClass("invisible");
	$("#icon-step3").addClass("invisible");
}

// Go to step 3
btn_step3.onclick = function() {
	// hide old forms
	$("#step1Rules").addClass("d-none");
	$("#step2Rules").addClass("d-none");
	// show new form
	$("#step3Rules").removeClass("d-none"); 
	// select button
	$("#step3").addClass("btn-selected");
	// unselect other buttons
	$("#step1").removeClass("btn-selected");
	$("#step2").removeClass("btn-selected");
	// add icon
	$("#icon-step3").removeClass("invisible");
	// remove icons
	$("#icon-step1").addClass("invisible");
	$("#icon-step2").addClass("invisible");
}



})

