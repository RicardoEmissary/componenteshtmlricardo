var Paqueterias = ['Fedex', 'ReadPack', 'Dhl', 'Paquete Express', 'Quiken', 'Sendex', 'Carssa'];
var PaqueteriasColores = ['rgb(124, 19, 176)', 'rgb(235, 44, 44)', 'rgb(255, 245, 29)', 'rgb(35, 12, 117)', 'rgb(98, 61, 232)', 'rgb(142, 12, 12)', 'rgb(75, 69, 131)'];
var PaqueteriasData = [2.0, 1.5, 1.5, 1.0, 0.5, 2.0, 1.0];
var ctx = document.getElementById("graphic-bar").getContext("2d");

var myGraphicBar = new Chart(ctx, {
    type: "bar",
    data: {
        labels: Paqueterias,
        datasets: [{
            label: 'Envios',
            data: PaqueteriasData,
            backgroundColor: PaqueteriasColores,
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});