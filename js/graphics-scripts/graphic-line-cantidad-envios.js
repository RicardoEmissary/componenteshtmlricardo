var Dias = ['Abril 1', 'Abril 2', 'Abril 3', 'Abril 4', 'Abril 5'];
var EnviosData = [3, 5, 2, 1, 4, 6, 7];
var ctx = document.getElementById("graphic-line-cantidad-envios").getContext("2d");
var myGraphicBar = new Chart(ctx, {
    type: "line",
    data: {
        labels: Dias,
        datasets: [{
            data: EnviosData,
            label: "Envios",
            borderColor: "#8e5ea2",
            backgroundColor: '#ffffff', // Add custom color background (Points and Fill)
            borderWidth: 2, // Specify bar border width
            fill: false
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});