var Meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo'];
var EnviosData = [300, 500, 200, 100, 400];
var ctx = document.getElementById("graphic-line-envios-por-mes").getContext("2d");
var myGraphicBar = new Chart(ctx, {
    type: "line",
    data: {
        labels: Meses,
        datasets: [{
            data: EnviosData,
            label: "Total envíos por mes",
            borderColor: "#ffffff",
            backgroundColor: '#ffffff', // Add custom color background (Points and Fill)
            borderWidth: 2, // Specify bar border width
            fill: false
        }]
    },
    options: {
        legend: {
            labels: {
                // This more specific font property overrides the global property
                fontColor: 'white'
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    fontColor: 'white',
                    beginAtZero: true
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: 'white',
                    beginAtZero: true
                }
            }]
        }
    }
});