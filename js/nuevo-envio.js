$(document).ready(function() {


    /*Funciones*/

    /*Metodo para mostrar y ocultar form con la informacion*/
    function MostrarDatosDireccion($containDatosP, $containOculto, //ID del area de los datos y formulario a ocultar
        $nombreP, $calleYnumeroP, $coloniaP, $codigopostalP, $referenciaP, $ciudadP, $estadoP, $paisP, $correoP, $telefonoP, //ID de los P con los datos
        $txtNombre, $txtCorreo, $txtTelefono, $txtCalleyNumero, $txtColonia, $txtReferencia, $txtCiudad, $txtEstado, $txtCodigoPostal, $txtPais) { /*ID de los input de los formularios*/
        cont++;
        $containDatosP.toggle('.1s');
        if (cont % 2) {
            /*Asignamos los datos a los input*/
            $txtNombre.val($nombreP.text());
            $txtCorreo.val($correoP.text());
            $txtTelefono.val($telefonoP.text());
            $txtCalleyNumero.val($calleYnumeroP.text());
            $txtColonia.val($coloniaP.text());
            $txtReferencia.val($referenciaP.text());
            $txtCiudad.val($ciudadP.text());
            $txtEstado.val($estadoP.text());
            $txtCodigoPostal.val($codigopostalP.text());
            $txtPais.val($paisP.text());

            $containOculto.css('display', 'none');
        } else {
            /*Asignamos los datos a los parrafos*/
            $nombreP.text($txtNombre.val());
            $correoP.text($txtCorreo.val());
            $telefonoP.text($txtTelefono.val());
            $calleYnumeroP.text($txtCalleyNumero.val());
            $coloniaP.text($txtColonia.val());
            $referenciaP.text($txtReferencia.val());
            $ciudadP.text($txtCiudad.val());
            $estadoP.text($txtEstado.val());
            $codigopostalP.text($txtCodigoPostal.val());
            $paisP.text($txtPais.val());

            $containOculto.css('display', 'block');
            cont = 0;
        }
    }


    /*Click para la transferencia de pantallas dentro de la pagina nuevos envios*/
    $('#btn-Remitente-Destinatario').click(function() {
        $('#form-remitente-destinatario').css("display", "none");
        $('#form-envio-informacion').css("display", "flex");
        $('#envio-resumen-envio').css("display", "none");
    });
    $('#btn-envio-caracteristicas-paquete').click(function() {
        $('#form-remitente-destinatario').css("display", "none");
        $('#form-envio-informacion').css("display", "none");
        $('#envio-resumen-envio').css("display", "flex");
    });

    /*---------------------------------------------------------------------------*/

    /*botones para regresar*/
    /*btn regresar en la pantalla resumen envio resumen*/
    $('#regresar-a-caracteristicas').click(function() {
        $('#form-remitente-destinatario').css("display", "none");
        $('#form-envio-informacion').css("display", "flex");
        $('#envio-resumen-envio').css("display", "none");

        /*Autocompletado de info a pantalla de caracteristicas paquete*/
        /*Remitente*/
        $('#nombre-remitente').text($('#nombre-remitente-resumen').text());
        $('#calle-y-numero-remitente').text($('#calle-y-numero-remitente-resumen').text());
        $('#colonia-remitente').text($('#colonia-remitente-resumen').text());
        $('#codigo-postal-remitente').text($('#codigo-postal-remitente-resumen').text());
        $('#referencia-remitente').text($('#referencia-remitente-resumen').text());
        $('#ciudad-remitente').text($('#ciudad-remitente-resumen').text());
        $('#Estado-remitente').text($('#Estado-remitente-resumen').text());
        $('#correo-remitente').text($('#correo-remitente-resumen').text());
        $('#telefono-remitente').text($('#telefono-remitente-resumen').text());
        $('#país-remitente').text($('#país-remitente-resumen').text());

        /*Destinatario*/
        $('#nombre-destinatario').text($('#nombre-destinatario-resumen').text());
        $('#calle-y-numero-destinatario').text($('#calle-y-numero-destinatario-resumen').text());
        $('#colonia-destinatario').text($('#colonia-destinatario-resumen').text());
        $('#codigo-postal-destinatario').text($('#codigo-postal-destinatario-resumen').text());
        $('#referencia-destinatario').text($('#referencia-destinatario-resumen').text());
        $('#ciudad-destinatario').text($('#ciudad-destinatario-resumen').text());
        $('#Estado-destinatario').text($('#Estado-destinatario-resumen').text());
        $('#correo-destinatario').text($('#correo-destinatario-resumen').text());
        $('#telefono-destinatario').text($('#telefono-destinatario-resumen').text());
        $('#país-destinatario').text($('#país-destinatario-resumen').text());

        /*Caracteristicas*/
        /*Falta paquete*/
        $('#cb_tipo-de-paquete').val($('#paquete').text());
        $('#txtLargo').val($('#largo').text());
        $('#txtAncho').val($('#ancho').text());
        $('#txtAlto').val($('#alto').text());
        $('#txtPeso').val($('#peso').text());
        $('#txtQue-envias').val($('#que-envia').text());
        $('#txtCantidad-asegurar').val($('#cantidad-asegurar').text());

    });

    /*---------------------------------------------------------------------------*/

    /*btn regresar en la pantalla caracteristicas*/
    $('#btn-regresar-remitente-destinatario').click(function() {
        $('#form-remitente-destinatario').css("display", "flex");
        $('#form-envio-informacion').css("display", "none");
        $('#envio-resumen-envio').css("display", "none");

        /*Autocompletado de info a pantalla remitente destinatario*/
        /*Remintente*/
        $('#txtNombre-Remitente-Pantalla').val($('#nombre-remitente').text());
        $('#txtCorreo-Remitente-Pantalla').val($('#correo-remitente').text());
        $('#txtTelefono-Remitente-Pantalla').val($('#telefono-remitente').text());
        $('#CalleYNumero-Remitente-Pantalla').val($('#calle-y-numero-remitente').text());
        $('#Colonia-Remitente-Pantalla').val($('#colonia-remitente').text());
        $('#Referencia-Remitente-Pantalla').val($('#referencia-remitente').text());
        $('#CodigoPostal-Remitente-Pantalla').val($('#codigo-postal-remitente').text());
        $('#Ciudad-Remitente-Pantalla').val($('#ciudad-remitente').text());
        $('#Estado-Remitente-Pantalla').val($('#Estado-remitente').text());
        $('#Pais-Remitente-Pantalla').val($('#país-remitente').text());

        /*Destinatario*/
        $('#txtNombre-Destinatario-Pantalla').val($('#nombre-destinatario').text());
        $('#txtCorreo-Destinatario-Pantalla').val($('#correo-destinatario').text());
        $('#txtTelefono-Destinatario-Pantalla').val($('#telefono-destinatario').text());
        $('#CalleYNumero-Destinatario-Pantalla').val($('#calle-y-numero-destinatario').text());
        $('#Colonia-Destinatario-Pantalla').val($('#colonia-destinatario').text());
        $('#Referencia-Destinatario-Pantalla').val($('#referencia-destinatario').text());
        $('#CodigoPostal-Destinatario-Pantalla').val($('#codigo-postal-destinatario').text());
        $('#Ciudad-Destinatario-Pantalla').val($('#ciudad-destinatario').text());
        $('#Estado-Destinatario-Pantalla').val($('#Estado-destinatario').text());
        $('#Pais-Destinatario-Pantalla').val($('#país-destinatario').text());
    });

    /*-------------------------------------PANTALLA DE REMITENTE Y DESTINATARIO--------------------------------*/
    $('#btn-Remitente-Destinatario').click(function() {
        /*Remitente*/
        $('#nombre-remitente').text($('#txtNombre-Remitente-Pantalla').val());
        $('#calle-y-numero-remitente').text($('#CalleYNumero-Remitente-Pantalla').val());
        $('#colonia-remitente').text($('#Colonia-Remitente-Pantalla').val());
        $('#codigo-postal-remitente').text($('#CodigoPostal-Remitente-Pantalla').val());
        $('#referencia-remitente').text($('#Referencia-Remitente-Pantalla').val());
        $('#ciudad-remitente').text($('#Ciudad-Remitente-Pantalla').val());
        $('#Estado-remitente').text($('#Estado-Remitente-Pantalla').val());
        $('#país-remitente').text($('#Pais-Remitente-Pantalla').val());
        $('#correo-remitente').text($('#txtCorreo-Remitente-Pantalla').val());
        $('#telefono-remitente').text($('#txtTelefono-Remitente-Pantalla').val());

        /*Destinatario*/
        $('#nombre-destinatario').text($('#txtNombre-Destinatario-Pantalla').val());
        $('#calle-y-numero-destinatario').text($('#CalleYNumero-Destinatario-Pantalla').val());
        $('#colonia-destinatario').text($('#Colonia-Destinatario-Pantalla').val());
        $('#codigo-postal-destinatario').text($('#CodigoPostal-Destinatario-Pantalla').val());
        $('#referencia-destinatario').text($('#Referencia-Destinatario-Pantalla').val());
        $('#ciudad-destinatario').text($('#Ciudad-Destinatario-Pantalla').val());
        $('#Estado-destinatario').text($('#Estado-Destinatario-Pantalla').val());
        $('#país-destinatario').text($('#Pais-Destinatario-Pantalla').val());
        $('#correo-destinatario').text($('#txtCorreo-Destinatario-Pantalla').val());
        $('#telefono-destinatario').text($('#txtTelefono-Destinatario-Pantalla').val());


    });

    /*-------------------------------------------PANTALLA DE CARACTERISTICA PAQUETE--------------------------------------*/

    var checked = 0;
    var checkedRecoleccion = 0;
    /*Cantidad asegurar*/
    $('#check-asegurar').click(function() {
        if (checked == 0) {
            $('#input-asegurar').css('display', 'block');
            $('#cantidad-asegurar-edit').css('display', 'block');
            $('#check-asegurar-edit').prop("checked", true);
            checked += 1;
        } else {
            $('#input-asegurar').css('display', 'none');
            $('#cantidad-asegurar-edit').css('display', 'none');
            $('#check-asegurar-edit').prop("checked", false);
            checked = 0;
        }
    });

    $('#check-asegurar-edit').click(function() {
        if (checked == 0) {
            $('#input-asegurar').css('display', 'block');
            $('#cantidad-asegurar-edit').css('display', 'block');
            $('#check-asegurar').prop("checked", true);
            checked += 1;
        } else {
            $('#input-asegurar').css('display', 'none');
            $('#cantidad-asegurar-edit').css('display', 'none');
            $('#check-asegurar').prop("checked", false);
            checked = 0;
        }
    });

    /*Check recolectar*/
    $('#check-recoleccion').click(function() {
        if (checkedRecoleccion == 0) {
            $('#check-recoleccion-edit').prop("checked", true);
            checkedRecoleccion += 1;
        } else {
            $('#check-recoleccion-edit').prop("checked", false);
            checkedRecoleccion = 0;
        }
    });

    $('#check-recoleccion-edit').click(function() {
        if (checkedRecoleccion == 0) {
            $('#check-recoleccion').prop("checked", true);
            checkedRecoleccion += 1;
        } else {
            $('#check-recoleccion').prop("checked", false);
            checkedRecoleccion = 0;
        }
    });

    var cont = 0; //variable para los click en editar de los forms de edición
    var contCaracteristicas = 0;
    var contResumen = 0;

    /*Click para editar los datos de remitente*/
    $('.editar-remitente').click(function() {
        MostrarDatosDireccion($('#remitente-muestra'), $('#card-destinatario-paqueteria'),
            /*ID de los div y p que muestran los datos*/
            $('#nombre-remitente'), $('#calle-y-numero-remitente'), $('#colonia-remitente'), $('#codigo-postal-remitente'), $('#referencia-remitente'), $('#ciudad-remitente'), $('#Estado-remitente'), $('#país-remitente'), $('#correo-remitente'), $('#telefono-remitente'),
            /*ID de los input que capturan los datos*/
            $('#txtNombre-remitente'), $('#txtCorreo-remitente'), $('#txtTelefono-remitente'), $('#txtCalleyNumero-remitente'), $('#txtColonia-remitente'), $('#txtReferencia-remitente'), $('#txtCiudad-remitente'), $('#txtEstado-remitente'), $('#txtCodigoPostal-remitente'), $('#CBPais-remitente')
        );
    });

    /*Click para editar los datos de destinatario*/
    $('.editar-destinatario').click(function() {
        MostrarDatosDireccion($('#destinatario-muestra'), $('#card-remitente-paqueteria'),
            /*ID de los div y p que muestran los datos*/
            $('#nombre-destinatario'), $('#calle-y-numero-destinatario'), $('#colonia-destinatario'), $('#codigo-postal-destinatario'), $('#referencia-destinatario'), $('#ciudad-destinatario'), $('#Estado-destinatario'), $('#país-destinatario'), $('#correo-destinatario'), $('#telefono-destinatario'),
            /*ID de los input que capturan los datos*/
            $('#txtNombre-destinatario'), $('#txtCorreo-destinatario'), $('#txtTelefono-destinatario'), $('#txtCalleyNumero-destinatario'), $('#txtColonia-destinatario'), $('#txtReferencia-destinatario'), $('#txtCiudad-destinatario'), $('#txtEstado-destinatario'), $('#txtCodigoPostal-destinatario'), $('#CBPais-destinatario')
        );
    });

    $('#btn-envio-caracteristicas-paquete').click(function() {
        /*Remitente*/
        $('#nombre-remitente-resumen').text($('#nombre-remitente').text());
        $('#calle-y-numero-remitente-resumen').text($('#calle-y-numero-remitente').text());
        $('#colonia-remitente-resumen').text($('#colonia-remitente').text());
        $('#codigo-postal-remitente-resumen').text($('#codigo-postal-remitente').text());
        $('#referencia-remitente-resumen').text($('#referencia-remitente').text());
        $('#ciudad-remitente-resumen').text($('#ciudad-remitente').text());
        $('#Estado-remitente-resumen').text($('#Estado-remitente').text());
        $('#país-remitente-resumen').text($('#país-remitente').text());
        $('#correo-remitente-resumen').text($('#correo-remitente').text());
        $('#telefono-remitente-resumen').text($('#telefono-remitente').text());

        /*Destinatario*/
        $('#nombre-destinatario-resumen').text($('#nombre-destinatario').text());
        $('#calle-y-numero-destinatario-resumen').text($('#calle-y-numero-destinatario').text());
        $('#colonia-destinatario-resumen').text($('#colonia-destinatario').text());
        $('#codigo-postal-destinatario-resumen').text($('#codigo-postal-destinatario').text());
        $('#referencia-destinatario-resumen').text($('#referencia-destinatario').text());
        $('#ciudad-destinatario-resumen').text($('#ciudad-destinatario').text());
        $('#Estado-destinatario-resumen').text($('#Estado-destinatario').text());
        $('#país-destinatario-resumen').text($('#país-destinatario').text());
        $('#correo-destinatario-resumen').text($('#correo-destinatario').text());
        $('#telefono-destinatario-resumen').text($('#telefono-destinatario').text());

        /*Caracteristicas*/
        $('#paquete').text($('#cb_tipo-de-paquete').val());
        $('#largo').text($('#txtLargo').val());
        $('#ancho').text($('#txtAncho').val());
        $('#alto').text($('#txtAlto').val());
        $('#peso').text($('#txtPeso').val());
        $('#que-envia').text($('#txtQue-envias').val());
        if ($('#check-asegurar').prop('checked') || $('#check-asegurar-edit').prop('checked')) {
            $('#contain-cantidad-asegurar').css('display', 'flex');
            $('#cantidad-asegurar').text($('#txtCantidad-asegurar').val());
        }


    });

    /*-----------------------------------------------------------------PANTALLA ENVIO RESUMEN----------------------------------------------------------------- */

    /*Click para editar los datos de las caracteristicas del paquete*/
    $('.editar-caracteristicas').click(function() {
        contCaracteristicas++;
        $('#caracteristica-muestra').toggle('.1s');
        if (contCaracteristicas % 2) {
            $('#CBPais-paquete').val($('#paquete').text());
            $('#txtLargo-caracteristica').val($('#largo').text());
            $('#txtAncho-caracteristica').val($('#ancho').text());
            $('#txtAlto-caracteristica').val($('#alto').text());
            $('#txtPeso-caracteristica').val($('#peso').text());
            $('#txtQue-envias-edit').val($('#que-envia').text());
            $('#txtCantidad-asegurar-edit').val($('#cantidad-asegurar').text());

            $('#card-destinatario-resumen').css('display', 'none');
            $('#card-remitente-resumen').css('display', 'none');
        } else {
            $('#paquete').text($('#CBPais-paquete').val());
            $('#largo').text($('#txtLargo-caracteristica').val());
            $('#ancho').text($('#txtAncho-caracteristica').val());
            $('#alto').text($('#txtAlto-caracteristica').val());
            $('#peso').text($('#txtPeso-caracteristica').val());
            $('#que-envia').text($('#txtQue-envias-edit').val());
            $('#cantidad-asegurar').text($('#txtCantidad-asegurar-edit').val());

            $('#card-destinatario-resumen').css('display', 'block');
            $('#card-remitente-resumen').css('display', 'block');
            contCaracteristicas = 0;
        }
    });

    /*Click para editar los datos de remitente*/
    $('.editar-remitente-resumen').click(function() {
        MostrarDatosDireccion($('#remitente-muestra-resumen'), $('#card-destinatario-resumen'),
            /*ID de los div y p que muestran los datos*/
            $('#nombre-remitente-resumen'), $('#calle-y-numero-remitente-resumen'), $('#colonia-remitente-resumen'), $('#codigo-postal-remitente-resumen'), $('#referencia-remitente-resumen'), $('#ciudad-remitente-resumen'), $('#Estado-remitente-resumen'), $('#país-remitente-resumen'), $('#correo-remitente-resumen'), $('#telefono-remitente-resumen'),
            /*ID de los input que capturan los datos*/
            $('#txtNombre-remitente-resumen'), $('#txtCorreo-remitente-resumen'), $('#txtTelefono-remitente-resumen'), $('#txtCalleyNumero-remitente-resumen'), $('#txtColonia-remitente-resumen'), $('#txtReferencia-remitente-resumen'), $('#txtCiudad-remitente-resumen'), $('#txtEstado-remitente-resumen'), $('#txtCodigoPostal-remitente-resumen'), $('#CBPais-remitente-resumen')
        );
        contResumen++;
        if (contResumen == 1) {
            $('#card-caracteristicas').css('display', 'none');
        } else {
            $('#card-caracteristicas').css('display', 'block');
            contResumen = 0;
        }
    });

    /*Click para editar los datos de destinatario*/
    $('.editar-destinatario-resumen').click(function() {
        MostrarDatosDireccion($('#destinatario-muestra-resumen'), $('#card-remitente-resumen'),
            /*ID de los div y p que muestran los datos*/
            $('#nombre-destinatario-resumen'), $('#calle-y-numero-destinatario-resumen'), $('#colonia-destinatario-resumen'), $('#codigo-postal-destinatario-resumen'), $('#referencia-destinatario-resumen'), $('#ciudad-destinatario-resumen'), $('#Estado-destinatario-resumen'), $('#país-destinatario-resumen'), $('#correo-destinatario-resumen'), $('#telefono-destinatario-resumen'),
            /*ID de los input que capturan los datos*/
            $('#txtNombre-destinatario-resumen'), $('#txtCorreo-destinatario-resumen'), $('#txtTelefono-destinatario-resumen'), $('#txtCalleyNumero-destinatario-resumen'), $('#txtColonia-destinatario-resumen'), $('#txtReferencia-destinatario-resumen'), $('#txtCiudad-destinatario-resumen'), $('#txtEstado-destinatario-resumen'), $('#txtCodigoPostal-destinatario-resumen'), $('#CBPais-destinatario-resumen')
        );
        contResumen++;
        if (contResumen == 1) {
            $('#card-caracteristicas').css('display', 'none');
        } else {
            $('#card-caracteristicas').css('display', 'block');
            contResumen = 0;
        }
    });

});